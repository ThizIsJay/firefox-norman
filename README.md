# Firefox - The Good Profile

* The Good Firefox Profile

This profile features an easy and clean firefox interface and good spyware and tracking protection and adblock

# Install Instructions

* Open firefox and go to
````
about:profile
````

Make an new profile and then click on open root directory
Delete all files in this profile and replace then with the files from this repository

* Restart Firefox and Enjoy
